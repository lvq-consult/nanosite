const {blue} = require('colors');

class Fixture {
  /**
   * @type {string}
   */
  _filepath;
  /**
   * @type {string}
   */
  _expected;
  /**
   * @type {(replacementToken: string, replacementValue: string, content: string) => string | undefined}
   */
  _transformer;
  /**
   * @type {string | undefined}
   */
  _transformedValue;

  /**
   * 
   * @param {string} filepath 
   * @param {string} expected 
   * @param {(actualContent: string, expectedContent: string) => string | undefined} transformer
   */
  constructor(filepath, expected, transformer) {
    this._filepath = filepath;
    this._expected = expected;
    this._transformer = transformer !== undefined && typeof transformer === 'function' ? (actualContent, expectedContent) => {
      if (this._transformedValue === undefined) {
        this._transformedValue = transformer(actualContent, expectedContent);
      }

      return this._transformedValue
    } : () => expected;
  }

  /**
   * @type {string}
   */
  get filepath() {
    return this._filepath;
  }

  /**
   * 
   * @param {string} actual 
   * @returns {string}
   */
  getExpectedValue(actual) {
    return this._transformer(actual, this._expected);
  }
}

const buildNumberTransformer = (actualContent, expectedContent) => {
  const buildNumberMatcher = new RegExp(/<meta name="build" content="(\d+)">/g);
  const matches = buildNumberMatcher.exec(actualContent);
  const build = matches !== null ? matches[1] : '';
  const expected = expectedContent.replace(/{{build}}/g, build);

  return expected;
};

/**
 * @type {Fixture[]}
 */
const TEST_FIXTURES = [
  new Fixture(
    './index.html',
    `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>My Website!</title><link rel="stylesheet" href="./style.css?b={{build}}" type="text/css"><script type="text/javascript" src="./js/app.js?b={{build}}"></script><meta name="env" content="production"><meta name="build" content="{{build}}"></head><body><h1>Welcome to your site!</h1><p>This is production!</p><a href="./blog">Go to blog!</a></body></html>`,
    (actualContent, expectedContent) => buildNumberTransformer(actualContent, expectedContent)
  ),
  new Fixture(
    './style.css',
    `body{font-size:16px}
//# sourceMappingURL=style.css.map`
  ),
  new Fixture(
    './style.css.map',
    `{"version":3,"sources":["style.css.map"],"names":[],"mappings":"AAAA,I,CACE,c","file":"style.css.map","sourcesContent":["body {\\n  font-size: 16px;\\n}"]}`
  ),
  new Fixture(
    './js/app.js',
    `console.log("Hello world");
//# sourceMappingURL=app.js.map`
  ),
  new Fixture(
    './js/app.js.map',
    `{"version":3,"sources":["0"],"names":["console","log"],"mappings":"AAAAA,QAAQC,IAAI","file":"app.js"}`
  ),
  new Fixture(
    './js/livereload.js',
    `window.LiveReloadOptions={host:(location.host||"localhost").split(":")[0],port:35729},document.write(\`<script src="//\${(location.host||"localhost").split(":")[0]}:35729/livereload.js?snipver=1"><\\/script>\`);
//# sourceMappingURL=livereload.js.map`
  ),
  new Fixture(
    './js/livereload.js.map',
    `{"version":3,"sources":["0"],"names":["window","LiveReloadOptions","host","location","split","port","document","write"],"mappings":"AAAAA,OAAOC,kBAAoB,CACzBC,MAAOC,SAASD,MAAQ,aAAaE,MAAM,KAAK,GAChDC,KAAM,OAGRC,SAASC,MAAM,mBAAmBJ,SAASD,MAAQ,aAAaE,MAAM,KAAK","file":"livereload.js"}`
  ),
  new Fixture(
    './blog/index.html', 
    `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>My Website!</title><link rel="stylesheet" href="./style.css?b={{build}}" type="text/css"><script type="text/javascript" src="./js/app.js?b={{build}}"></script><meta name="env" content="production"><meta name="build" content="{{build}}"></head><body><h1>Welcome to your blog!</h1></body></html>`,
    (actualContent, expectedContent) => buildNumberTransformer(actualContent, expectedContent)
  ),
  new Fixture(
    './blog/article-1.html', 
    `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>My Website!</title><link rel="stylesheet" href="./style.css?b={{build}}" type="text/css"><script type="text/javascript" src="./js/app.js?b={{build}}"></script><meta name="env" content="production"><meta name="build" content="{{build}}"></head><body><h1>Article 1</h1></body></html>`,
    (actualContent, expectedContent) => buildNumberTransformer(actualContent, expectedContent)
  ),
  new Fixture(
    './da/blog/index.html',
    `<!DOCTYPE html><html lang="da"><head><meta charset="utf-8"><title>Mit website!</title><link rel="stylesheet" href="./style.css?b={{build}}" type="text/css"><script type="text/javascript" src="./js/app.js?b={{build}}"></script><meta name="env" content="production"><meta name="build" content="{{build}}"></head><body><h1>Velkommen til din blog!</h1></body></html>`,
    (actualContent, expectedContent) => buildNumberTransformer(actualContent, expectedContent)
  ),
  new Fixture(
    './da/blog/article-1.html',
    `<!DOCTYPE html><html lang="da"><head><meta charset="utf-8"><title>Mit website!</title><link rel="stylesheet" href="./style.css?b={{build}}" type="text/css"><script type="text/javascript" src="./js/app.js?b={{build}}"></script><meta name="env" content="production"><meta name="build" content="{{build}}"></head><body><h1>Artikel 1</h1></body></html>`,
    (actualContent, expectedContent) => buildNumberTransformer(actualContent, expectedContent)
  ),
  new Fixture(
    './da/index.html',
    `<!DOCTYPE html><html lang="da"><head><meta charset="utf-8"><title>Mit website!</title><link rel="stylesheet" href="./style.css?b={{build}}" type="text/css"><script type="text/javascript" src="./js/app.js?b={{build}}"></script><meta name="env" content="production"><meta name="build" content="{{build}}"></head><body><h1>Velkommen til dit site!</h1><p>This is production!</p><a href="./blog">Gå til blog</a></body></html>`,
    (actualContent, expectedContent) => buildNumberTransformer(actualContent, expectedContent)
  ),
  new Fixture('./files/terms.txt', 'terms'),
  new Fixture('./files/manuscript.md', 'manuscript'),
  new Fixture('./images/subfolder/keep.jpg', 'keep'),
  new Fixture('robots.txt', `User-agent: *
Allow: /

Sitemap: https://example.com/sitemap.txt
`),
  new Fixture('sitemap.txt', `http://localhost:5000/blog/article-1.html
http://localhost:5000/blog/index.html
http://localhost:5000/da/blog/article-1.html
http://localhost:5000/da/blog/index.html
http://localhost:5000/da/index.html
http://localhost:5000/index.html`)
];

module.exports = {
  Fixture,
  TEST_FIXTURES
};
