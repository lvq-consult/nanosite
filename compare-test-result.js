const { readFile } = require('fs-extra');
const { resolve } = require('path');
const { TEST_FIXTURES, Fixture } = require('./test-fixtures');
const { red, green } = require('colors');

class TestError extends Error {
  /**
   * @type {string}
   */
  _actual;

  /**
   *
   * @param {string} message
   * @param {string} actual
   */
  constructor(message, actual) {
    super(message);

    this._actual = actual;
  }

  /**
   * @type {string}
   */
  get actual() {
    return this._actual;
  }
}

/**
 * @param {string} baseDir
 * @param {Fixture} fixture
 * @returns {string}
 */
async function compareFixture(baseDir, fixture) {
  const actual = await readFile(resolve(baseDir, fixture.filepath), { encoding: 'utf-8' });
  const expected = fixture.getExpectedValue(actual);

  if (expected !== actual) {
    throw new TestError('', actual);
  }

  return actual;
}

/**
 *
 * @param {Fixture} fixture
 */
function reportPass(fixture) {
  console.log(green(`[✓] ${fixture.filepath}`));
}

/**
 *
 * @param {Fixture} fixture
 * @param {TestError} e
 */
function reportFailue(fixture, e) {
  console.log(red(`[x] ${fixture.filepath}`));

  if (e instanceof TestError) {
    console.log(red('Expected:'));
    console.log(red(fixture.getExpectedValue(e.actual)));
    console.log(red('Actual:'));
    console.log(red(e.actual));
  }

  if (!(e instanceof TestError)) {
    console.log(red(`${e.name}: ${e.message}`));
    console.log(red(e.stack));
  }
}

async function main() {
  const BASE_DIR = './test-dist';
  let passes = 0;
  let failures = 0;
  const start = new Date().getTime();

  for (const fixture of TEST_FIXTURES) {
    try {
      const actual = await compareFixture(BASE_DIR, fixture);

      reportPass(fixture);
      passes++;
    } catch (e) {
      reportFailue(fixture, e);
      failures++;
    }
  }

  console.log(
    `Ran for ${new Date().getTime() - start}ms. ${passes} ${passes === 1 ? 'test' : 'tests'} passed and ${failures} ${
      failures === 1 ? 'test' : 'tests'
    } failed.`,
  );
}

main();
