# Nanosite

Extremely simple, extremely oppinionated static site generator.

To get going run:

```bash
npm init
npm i --save nanosite
npx nanosite init
npx nanosite start
```

And you hit the ground running!

## Folder example

```
src
├─ assets
│  ├─ images
│  ├─ js
│  │  └─ app.js
│  └─ style
│     └─ style.scss
├─ pages
│  ├─ blog
│  │  ├─ article-1.pug
│  │  └─ index.pug
│  └─ index.pug
├─ partials
│  └─ _layout.pug
├─ translations.json
└─ context.js
```

## Commands

```bash
nanosite [command]

Commands:
  nanosite build [inputPath] [outputPath]            Build your site
  nanosite serve [inputPath]                         Serve up the site
  nanosite watch [inputPath] [outputPath] [pattern]  Watch for changes and build when detected
  nanosite start [inputPath] [outputPath] [pattern]  Watch and serve the site
  nanosite init [outputPath]                         Bootstrap a new site

Options:
  --version  Show version
  --help    Show help
```
