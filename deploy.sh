#!/bin/bash

INPUT=$1
SEMVERTYPE=${INPUT:-"patch"}

if [ "${SEMVERTYPE}" != "major" ] && [ "${SEMVERTYPE}" != "minor" ] && [ "${SEMVERTYPE}" != "patch" ];
then
  echo "Invalid semver string"
  exit -1
fi

npm run build
npm run test
npm version $SEMVERTYPE

git push
npm publish
