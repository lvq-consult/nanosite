import { yellow } from 'colors';
import { mkdirp, writeFile } from 'fs-extra';
import { resolve } from 'path';
import { contextualLogger } from './build/log.fn';

const { log } = contextualLogger('🔥 init', yellow);

enum FileTypes {
  File,
  Folder,
}

type KnownFileTypes = 'pug' | 'md' | '' | 'scss' | 'js' | 'gitkeep' | 'json' | 'txt' | 'jpg';

interface FileSystemEntity {
  type: FileTypes;
}

interface File extends FileSystemEntity {
  type: FileTypes.File;
  name: string;
  extension?: KnownFileTypes;
  contents?: string;
}

interface Folder extends FileSystemEntity {
  type: FileTypes.Folder;
  name: string;
  files?: FileSystemEntity[];
}

function folder(name: string, files: FileSystemEntity[] = []): Folder {
  return {
    type: FileTypes.Folder,
    name,
    files,
  };
}

function file(name: string, extension: KnownFileTypes, contents = ''): File {
  return {
    type: FileTypes.File,
    name,
    extension,
    contents,
  };
}

const indexContents = `extends ../partials/_layout.pug
block content
  h1 #{t.headline}
  p #{ctx.test}
  a(href="./blog") #{t.blogLink}
`;
const layoutContents = `doctype html
html(
  lang=lang
  )
  head
    meta(charset='utf-8')
    title #{t.title}
    link(
      rel='stylesheet',
      href='./style.css?b=' + ctx.build,
      type='text/css'
    )
    if ctx.env === 'development'
      script(
        type='text/javascript',
        src='./js/livereload.js'
      )
    script(
      type='text/javascript',
      src='./js/app.js?b=' + ctx.build
    )
    meta(name='env', content=ctx.env)
    meta(name='build', content=ctx.build)
    block head
  body
    block content
    block foot
`;
const readmeContents = `# Nanosite

Extremely simple, extremely oppinionated static site generator.

To get going run:

\`\`\`bash
npm init
npm i --save nanosite
npx nanosite init
npx nanosite start
\`\`\`

And you hit the ground running!



## Folder example

\`\`\`
src
├─ assets
│  ├─ files
│  │  ├─ terms.txt
│  │  └─ manuscript.md
│  ├─ images
│  │  └─ subfolder
│  │     └─ .gitkeep
│  ├─ js
│  │  └─ app.js
│  └─ style
│     └─ style.scss
├─ pages
│  ├─ blog
│  │  ├─ article-1.pug
│  │  └─ index.pug
│  └─ index.pug
├─ partials
│  └─ _layout.pug
├─ translations.json
└─ context.js
\`\`\`

## Commands

\`\`\`bash
nanosite [command]

Commands:
  nanosite build [inputPath] [outputPath]            Build your site
  nanosite serve [inputPath]                         Serve up the site
  nanosite watch [inputPath] [outputPath] [pattern]  Watch for changes and build when detected
  nanosite start [inputPath] [outputPath] [pattern]  Watch and serve the site
  nanosite init [outputPath]                         Bootstrap a new site

Options:
  --version  Show version
  --help    Show help
\`\`\`
`;

const livereloadContents = `window.LiveReloadOptions = {
  host: (location.host || 'localhost').split(':')[0],
  port: 35729,
};

document.write(\`<script src="//\${(location.host || 'localhost').split(':')[0]}:35729/livereload.js?snipver=1"></script>\`);
`;
const styleContents = `body {
  font-size: 16px;
}
`;

const appContents = `console.log('Hello world');
`;

const blogIndexContents = `extends ../../partials/_layout.pug
block content
  h1 #{t.blog.title}
`;
const blogArticleOneContents = `extends ../../partials/_layout.pug
block content
  h1 #{t.article1.title}
`;

const contextJsContents = `const build = new Date().getTime();

/**
 * Put some template logic in here
 *
 * @param {"development" | "production"} environment
 * @param {string} path
 * @param {string | undefined} language
 * @returns {Promise<{ [key: string]: any }>}
 */
module.exports = async function getContext(environment, path, language) {
  const defaultContext = {
    env: environment,
    build,
  };

  return environment === "development" ? {
    ...defaultContext,
    "test": "This is development!"
  } : {
    ...defaultContext,
    "test": "This is production!"
  };
}
`;

const translationsJsonContents = `{
  "default": "en",
  "en": {
    "title": "My Website!",
    "headline": "Welcome to your site!",
    "blogLink": "Go to blog!",
    "blog": {
      "title": "Welcome to your blog!"
    },
    "article1": {
      "title": "Article 1"
    }
  },
  "da": {
    "title": "Mit website!",
    "headline": "Velkommen til dit site!",
    "blogLink": "Gå til blog",
    "blog": {
      "title": "Velkommen til din blog!"
    },
    "article1": {
      "title": "Artikel 1"
    }
  }
}
`;

const robotsTxtContents = `User-agent: *
Allow: /

Sitemap: https://example.com/sitemap.txt
`;

const INIT_TREE: FileSystemEntity[] = [
  folder('assets', [
    folder('files', [file('terms', 'txt', 'terms'), file('manuscript', 'md', 'manuscript')]),
    folder('images', [folder('subfolder', [file('keep', 'jpg', 'keep')])]),
    folder('js', [file('app', 'js', appContents), file('livereload', 'js', livereloadContents)]),
    folder('style', [file('style', 'scss', styleContents)]),
    file('robots', 'txt', robotsTxtContents),
  ]),
  folder('pages', [
    file('index', 'pug', indexContents),
    folder('blog', [file('index', 'pug', blogIndexContents), file('article-1', 'pug', blogArticleOneContents)]),
  ]),
  folder('partials', [file('_layout', 'pug', layoutContents)]),
  file('README', 'md', readmeContents),
  file('context', 'js', contextJsContents),
  file('translations', 'json', translationsJsonContents),
];

async function handleFile(f: File, path: string, dryRun = false) {
  const filename = `${f.name}.${f.extension}`;
  const filepath = resolve(path, filename);

  log(`Creating file ${filepath}`);

  if (!dryRun) {
    await writeFile(filepath, f.contents, { encoding: 'utf-8' });
  }
}

async function handleFolder(f: Folder, basePath: string, dryRun = false) {
  const path = resolve(basePath, f.name);

  log(`Creating folder ${path}`);

  if (!dryRun) {
    await mkdirp(path);
  }

  if (f.files.length) {
    log(`Found ${f.files.length} ${f.files.length === 1 ? 'file' : 'files'} to create in folder.`);

    return await parseTree(f.files, path);
  }
}

async function parseTree(tree: FileSystemEntity[], basePath: string, dryRun = false) {
  await mkdirp(basePath);

  return await Promise.all(
    tree.map(async entity => {
      switch (entity.type) {
        case FileTypes.File:
          return await handleFile(entity as File, basePath);
        case FileTypes.Folder:
          return await handleFolder(entity as Folder, basePath);
      }
    }),
  );
}

export async function initCommand(outputPath: string, dryRun = false) {
  await parseTree(INIT_TREE, outputPath, dryRun);
}
