import { magenta } from 'colors';
import { once } from 'events';
import { createServer } from 'livereload';
import { contextualLogger } from './build/log.fn';

const { log } = contextualLogger('live-reload', magenta);

export function livereloadCommand(inputPath: string) {
  const server = createServer({
    port: 35729,
    noListen: true,
  });

  let hasSeenListeningEvent = false;

  return {
    listen() {
      return new Promise<void>(resolve => {
        hasSeenListeningEvent = false;

        once(server, 'listening').then(() => {
          hasSeenListeningEvent = true;
          log('listening on http://localhost:35729');
          resolve();
        });

        server.listen();
      });
    },
    stop() {
      return new Promise<void>(resolve => {
        once(server, 'close').then(() => resolve());

        if (hasSeenListeningEvent) {
          server.close();
        } else {
          server.on('listening', () => server.close());
        }
      });
    },
    refresh(filepath: string) {
      log(`refresh ${filepath}`);
      server.refresh(filepath);
    },
  };
}
