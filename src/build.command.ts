import { rainbow, white } from 'colors';
import { Environments } from './build/environments.type';
import { generateSitemap } from './build/generate-sitemap.fn';
import { contextualLogger } from './build/log.fn';
import { packAssets } from './build/pack-assets.fn';
import { prepareDistFolder } from './build/prepare-dist-folder.fn';
import { renderLanguages } from './build/render-languages.fn';
import { executeCommands } from './utils/execute-commands.fn';

export async function buildCommand(
  environment: Environments,
  inputPath: string,
  outputPath: string,
  baseUrl?: string,
  postHooks?: string[],
) {
  const { error, createTimer } = contextualLogger(`🏗️ build ${rainbow(environment)}`, white);

  if (environment !== 'development' && environment !== 'production') {
    error(`Invalid environment must be either 'development' or 'production'`);
    process.exit(-1);
  }

  const timer = createTimer();

  await prepareDistFolder(environment, outputPath);
  await renderLanguages(environment, inputPath, outputPath);
  await packAssets(environment, inputPath, outputPath);

  if (baseUrl) {
    await generateSitemap(baseUrl, outputPath);
  }

  if (postHooks?.length > 0) {
    await executeCommands('⚓ postHook', postHooks);
  }

  timer.finish();
}
