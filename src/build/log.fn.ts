import { red } from 'colors';
import { Timer } from './timer';

export function contextualLogger(context: string, color: (text: string) => string) {
  return {
    log(message: string, ...args: any[]) {
      console.log(color(`[${new Date().toISOString()}] ${context}: ${message}`), ...args);
    },
    error(message: string, ...args: any[]) {
      console.log(red(`[${new Date().toISOString()}] ${context}: ${message}`), ...args);
    },
    createTimer(ctx?: string) {
      return new Timer(ctx ?? context, color);
    },
  };
}
