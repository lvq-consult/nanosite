import { magenta } from 'colors';
import { stat } from 'fs/promises';
import * as glob from 'glob';
import { contextualLogger } from './log.fn';

const { error } = contextualLogger('get files', magenta);

export async function getFiles(pattern: string, noDirectories = false) {
  const entries = await new Promise<string[]>((r, reject) =>
    glob(pattern, (err, matches) => (err != null ? reject(err) : r(matches))),
  );

  if (noDirectories) {
    const filteredEntries = [] as string[];

    for (const entry of entries) {
      try {
        const info = await stat(entry);

        if (info.isDirectory()) {
          continue;
        }

        filteredEntries.push(entry);
      } catch (e) {
        error(e?.message ?? e);
        continue;
      }
    }

    return filteredEntries;
  }

  return entries;
}
