import { blue } from 'colors';
import { resolve } from 'path';
import { copyFiles } from './copy-files.fn';
import { copyImages } from './copy-images.fn';
import { Environments } from './environments.type';
import { contextualLogger } from './log.fn';
import { processScripts } from './process-scripts.fn';
import { renderSass } from './render-sass.fn';

const { createTimer } = contextualLogger('💼 packing assets', blue);

export async function packAssets(environment: Environments, inputPath: string, distDir: string) {
  const timer = createTimer();
  const ASSETS_DIR = resolve(inputPath, './assets');
  const STYLE_DIR = resolve(ASSETS_DIR, './style');

  await renderSass(environment, distDir, STYLE_DIR);
  await copyImages(environment, distDir, ASSETS_DIR);
  await processScripts(environment, distDir, ASSETS_DIR);
  await copyFiles(environment, distDir, ASSETS_DIR);

  timer.finish();
}
