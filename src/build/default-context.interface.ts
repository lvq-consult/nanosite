export interface DefaultContext {
  t?: { [key: string]: any };
  lang?: string;
}
