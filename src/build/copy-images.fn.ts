import { cyan } from 'colors';
import { lstat } from 'fs';
import { copyFile, mkdirp } from 'fs-extra';
import { basename, resolve } from 'path';
import { Environments } from './environments.type';
import { getFiles } from './get-files.fn';
import { contextualLogger } from './log.fn';
import { retryWithDelay } from './retry-with-delay.fn';

const { createTimer, log, error } = contextualLogger('🖼️ copying images', cyan);

export async function copyImages(environment: Environments, distDir: string, assetsDir: string) {
  const timer = createTimer();
  const IMAGES_DIR = resolve(assetsDir, './images');
  const images = await getFiles(`${IMAGES_DIR}/**/*`, true);

  log(`Found ${images.length} ${images.length === 1 ? 'image' : 'images'} to copy`);

  const result = await Promise.all(
    images.map(async imagePath => {
      const filename = basename(imagePath);
      const imageTimer = createTimer(`- ${filename}`);

      try {
        const isDirectory = await new Promise<boolean>((r, reject) =>
          lstat(imagePath, (err, stats) => (err != null ? reject(err) : r(stats.isDirectory()))),
        );

        if (isDirectory) {
          log(`Did not copy '${imagePath}' it was a directory`);
          timer.finish();

          return;
        }
      } catch (e) {
        error(e.message, e);

        throw e;
      }

      const newPath = resolve(imagePath).replace(assetsDir, distDir);
      const basepath = newPath.replace(basename(newPath), '');

      await retryWithDelay(() => mkdirp(basepath));
      await retryWithDelay(() => copyFile(imagePath, newPath));

      imageTimer.finish();
    }),
  );

  timer.finish();

  return result;
}
