import { grey } from 'colors';
import { delay } from './delay.fn';
import { contextualLogger } from './log.fn';

const { log, error } = contextualLogger('retry-with-delay', grey);

export async function retryWithDelay<T>(task: () => Promise<T>, milliseconds = 100) {
  try {
    const result = await task();

    return result;
  } catch (e) {
    log(`Retrying task that failed with message: ${e.message}`);
    error(e.message, e);

    await delay(milliseconds);

    try {
      const result = await task();

      return result;
    } catch (e1) {
      error(e.message, e);
    }
  }
}
