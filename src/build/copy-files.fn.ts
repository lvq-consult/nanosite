import { magenta } from 'colors';
import { lstat } from 'fs';
import { copyFile, mkdirp } from 'fs-extra';
import { basename, resolve } from 'path';
import { Environments } from './environments.type';
import { getFiles } from './get-files.fn';
import { contextualLogger } from './log.fn';
import { retryWithDelay } from './retry-with-delay.fn';

const { createTimer, log, error } = contextualLogger('🗄️ copying files', magenta);

export async function copyFiles(environment: Environments, distDir: string, assetsDir: string) {
  const timer = createTimer();
  const FILES_DIR = resolve(assetsDir, './files');
  const filesInRoot = await getFiles(`${assetsDir}/*`, true);
  const filesInFolders = await getFiles(`${FILES_DIR}/**/*`, true);
  const files = [...filesInRoot, ...filesInFolders];

  log(`Found ${files.length} ${files.length === 1 ? 'file' : 'files'} to copy`);

  const result = await Promise.all(
    files.map(async filePath => {
      const filename = basename(filePath);
      const fileTimer = createTimer(`- ${filename}`);

      try {
        const isDirectory = await new Promise<boolean>((r, reject) =>
          lstat(filePath, (err, stats) => (err != null ? reject(err) : r(stats.isDirectory()))),
        );

        if (isDirectory) {
          log(`Did not copy '${filePath}' it was a directory`);
          fileTimer.finish();

          return;
        }
      } catch (e) {
        error(e.message, e);

        throw e;
      }

      const newPath = resolve(filePath).replace(assetsDir, distDir);
      const basepath = newPath.replace(basename(newPath), '');

      await retryWithDelay(() => mkdirp(basepath));
      await retryWithDelay(() => copyFile(filePath, newPath));

      fileTimer.finish();
    }),
  );

  timer.finish();

  return result;
}
