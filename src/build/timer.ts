import { blue } from 'colors';
import { contextualLogger } from './log.fn';

export class Timer {
  private start: number;

  constructor(private context: string, private color: (text: string) => string = blue) {
    this.start = new Date().getTime();
  }

  public get elapsed() {
    return new Date().getTime() - this.start;
  }

  public finish() {
    if (this.context) {
      contextualLogger(`${this.context}`, this.color).log(`Finished in ${this.elapsed}ms ⏱️`);
    }
  }
}
