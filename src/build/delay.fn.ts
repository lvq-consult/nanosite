import { white } from 'colors';
import { contextualLogger } from './log.fn';

const { log } = contextualLogger('delay', white);

export async function delay(milliseconds = 100) {
  log(`${milliseconds}ms`);

  return new Promise<void>(r => setTimeout(() => r(), milliseconds));
}
