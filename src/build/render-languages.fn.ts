import { cyan } from 'colors';
import { readJson } from 'fs-extra';
import { resolve } from 'path';
import { Environments } from './environments.type';
import { contextualLogger } from './log.fn';
import { renderPages } from './render-pages.fn';
import { TranslationsJson } from './translations-json.interface';

const { createTimer, log, error } = contextualLogger('🧏 rendering languages', cyan);

export async function renderLanguages(environment: Environments, inputPath: string, distDir: string) {
  const timer = createTimer();
  const getTranslations = async () => {
    try {
      const path = resolve(inputPath, 'translations.json');
      const translations: TranslationsJson = await readJson(path);

      return translations;
    } catch (e) {
      return null;
    }
  };
  const translations = await getTranslations();
  const getContextProvider: () => (
    environment: Environments,
    page: string,
    language?: string,
  ) => Promise<{ [key: string]: string }> = () => {
    try {
      return require(resolve(inputPath, 'context.js'));
    } catch (e) {
      error('Error retrieving context.js', e);

      return () => Promise.resolve({});
    }
  };
  const getContext = getContextProvider();

  if (translations === null) {
    log('Found no specific languages, rendering pages');
    await renderPages(environment, inputPath, distDir, (env, page) => getContext(env, page));
  } else {
    const languages = Object.keys(translations).filter(
      key => key !== ('default' as keyof TranslationsJson) && key !== translations.default,
    );
    log(`Found ${languages.length} languages`);
    const t1 = createTimer(`- ${translations.default}`);

    await renderPages(
      environment,
      inputPath,
      distDir,
      (env, page) => getContext(env, page, translations.default),
      { lang: translations.default, t: translations[translations.default] },
      true,
    );

    t1.finish();

    for (const language of languages) {
      const t2 = createTimer(`🧏  language ${language}`);
      const languageDistDir = resolve(distDir, language);

      await renderPages(
        environment,
        inputPath,
        languageDistDir,
        (env, page) => getContext(env, page, language),
        { lang: language, t: translations[language] },
        false,
      );
      t2.finish();
    }
  }

  timer.finish();
}
