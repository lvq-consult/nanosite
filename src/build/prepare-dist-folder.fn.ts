import { blue } from 'colors';
import { mkdirp, remove } from 'fs-extra';
import { resolve } from 'path';
import { delay } from './delay.fn';
import { Environments } from './environments.type';
import { getFiles } from './get-files.fn';
import { contextualLogger } from './log.fn';
import { retryWithDelay } from './retry-with-delay.fn';

const { createTimer, log, error } = contextualLogger('📁 preparing dist folder', blue);

export async function prepareDistFolder(environment: Environments, distDir: string) {
  const timer = createTimer();
  log(`Removing ${distDir}`);

  if (process.platform === 'win32') {
    const files = await getFiles(resolve(distDir, '**/*'));
    const result = await Promise.all(
      files.map(async file => {
        try {
          await retryWithDelay(() => remove(file));

          return { [file]: true };
        } catch (e) {
          log(`Unable to delete ${file}`);
          error(e.message, e);

          return { [file]: false };
        }
      }),
    ).then(r => r.reduce((obj, curr) => ({ ...obj, ...curr }), {}));

    const deletedFiles = Object.keys(result).filter(key => result[key]);
    const undeletedFiles = Object.keys(result).filter(key => result[key] === false);

    log(`Initially deleted ${deletedFiles.length} files`);

    if (undeletedFiles.length) {
      log(`Unable to deleted ${undeletedFiles.length}, delaying 5000ms and trying again...`);

      await delay(5000);

      for (const file of undeletedFiles) {
        try {
          await retryWithDelay(() => remove(file));
        } catch (e) {
          log(`Unable to delete ${file}`);
          error(e.message, e);
        }
      }
    }
  } else {
    try {
      await retryWithDelay(() => remove(distDir));
    } catch (e) {
      log(`Unable to delete ${distDir}`);
      error(e.message, e);
    }
  }

  log(`Creating ${distDir}`);
  await retryWithDelay(() => mkdirp(distDir));
  timer.finish();
}
