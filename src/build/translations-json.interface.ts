export interface TranslationsJson {
  default: string;
  [key: string]: any;
}
