import { magenta } from 'colors';
import { existsSync, writeFile } from 'fs-extra';
import { trimEnd, trimStart } from 'lodash';
import { getFiles } from './get-files.fn';
import { contextualLogger } from './log.fn';

const { log, error, createTimer } = contextualLogger('🗺️ generating sitemap', magenta);

export async function generateSitemap(baseUrl: string, distDir: string) {
  const timer = createTimer();

  const url = trimEnd(baseUrl, '/');
  const files = await getFiles(`${distDir}/**/*.html`);
  const urls = files
    .map(file => file.replace(distDir, ''))
    .map(file => trimStart(file, '/'))
    .map(file => `${url}/${file}`)
    .join('\n');

  log(`Found ${files.length} urls to write into sitemap`);

  const outputPath = `${trimEnd(distDir, '/')}/sitemap.txt`;

  if (existsSync(outputPath)) {
    const newOutputPath = `${outputPath}-${new Date().getTime()}`;
    error(`${outputPath} already exists, writing to ${newOutputPath} instead`);

    await writeFile(newOutputPath, urls);
  } else {
    await writeFile(outputPath, urls);
  }

  timer.finish();
}
