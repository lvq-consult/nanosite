import { green } from 'colors';
import { mkdirp, readFile, writeFile } from 'fs-extra';
import { basename, resolve } from 'path';
import { minify as minifyJs } from 'terser';
import { Environments } from './environments.type';
import { getFiles } from './get-files.fn';
import { contextualLogger } from './log.fn';
import { retryWithDelay } from './retry-with-delay.fn';

const { log, createTimer, error } = contextualLogger('📜 processing scripts', green);

export async function processScripts(environment: Environments, distDir: string, assetsDir: string) {
  const timer = createTimer();
  const SCRIPTS_DIR = resolve(assetsDir, './js');
  const scripts = await getFiles(`${SCRIPTS_DIR}/**/*`, true);

  log(`Found ${scripts.length} ${scripts.length === 1 ? 'script' : 'scripts'} to process`);

  const result = await Promise.all(
    scripts.map(async jsPath => {
      const filename = basename(jsPath);
      const jsTimer = createTimer(`- ${filename}`);

      try {
        const mapFilename = `${filename}.map`;
        const newPath = resolve(jsPath).replace(assetsDir, distDir);
        const basepath = newPath.replace(basename(newPath), '');
        const mapFilePath = resolve(basepath, mapFilename);

        await retryWithDelay(() => mkdirp(basepath));

        const contents = await readFile(jsPath, { encoding: 'utf-8' });

        if (environment === 'production') {
          const { code, map } = minifyJs(contents, {
            sourceMap: {
              filename,
              url: mapFilename,
            },
          });

          await writeFile(newPath, code, { encoding: 'utf-8' });
          await writeFile(mapFilePath, map, { encoding: 'utf-8' });
        } else {
          await writeFile(newPath, contents, { encoding: 'utf-8' });
        }
      } catch (e) {
        error(e.message, e);
      }

      jsTimer.finish();
    }),
  );

  timer.finish();

  return result;
}
