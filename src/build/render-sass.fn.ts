import { magenta } from 'colors';
import { minify as minifyCss } from 'csso';
import { writeFile } from 'fs-extra';
import { basename, resolve } from 'path';
import * as sass from 'sass';
import { Environments } from './environments.type';
import { contextualLogger } from './log.fn';

const { error, createTimer } = contextualLogger('🧁 rendering sass', magenta);

export async function renderSass(environment: Environments, distDir: string, styleDir: string) {
  const timer = createTimer();

  try {
    const mainSassFile = resolve(styleDir, './style.scss');
    const styleDistDir = mainSassFile.replace(styleDir, distDir);
    const newPath = styleDistDir.replace('.scss', '.css');
    const mapFilename = `${basename(newPath)}.map`;
    const mapFilepath = `${newPath}.map`;
    const sassRenderer = file =>
      new Promise<sass.Result>((r, reject) =>
        sass.render(
          {
            file,
          },
          (err, result) => (err != null ? reject(err) : r(result)),
        ),
      );

    const contents = await sassRenderer(mainSassFile);
    const cssBuffer = Buffer.from(contents.css);
    const renderedCss = cssBuffer.toString();

    if (environment === 'production') {
      const { css, map } = minifyCss(renderedCss, {
        sourceMap: true,
        filename: mapFilename,
      });
      const cssWithMapReference = `${css}\n//# sourceMappingURL=${mapFilename}`;

      await writeFile(newPath, cssWithMapReference, { encoding: 'utf-8' });
      await writeFile(mapFilepath, map.toString(), { encoding: 'utf-8' });
    } else {
      await writeFile(newPath, renderedCss, { encoding: 'utf-8' });
    }
  } catch (e) {
    error(e.message, e);
  }

  timer.finish();
}
