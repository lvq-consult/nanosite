import { yellow } from 'colors';
import { mkdirp, stat, writeFile } from 'fs-extra';
import { basename, resolve } from 'path';
import { compileFile } from 'pug';
import { DefaultContext } from './default-context.interface';
import { Environments } from './environments.type';
import { getFiles } from './get-files.fn';
import { contextualLogger } from './log.fn';
import { retryWithDelay } from './retry-with-delay.fn';

async function directoryExists(path) {
  try {
    return (await stat(path))?.isDirectory();
  } catch (e) {
    return false;
  }
}

const { log, createTimer, error } = contextualLogger('📄 rendering pages', yellow);

export async function renderPages(
  environment: Environments,
  inputPath: string,
  distDir: string,
  getContext: (
    environment: Environments,
    page: string,
  ) => Promise<{
    [key: string]: string;
  }>,
  defaultContext: DefaultContext = {},
  defaultLanguage = true,
) {
  const timer = createTimer();
  const PAGES_DIR = resolve(inputPath, './pages');
  const pages = await getFiles(`${PAGES_DIR}/**/*.pug`);

  log(`Found ${pages.length} ${pages.length === 1 ? 'page' : 'pages'} to render`);

  const result = await Promise.all(
    pages.map(async page => {
      try {
        const subdir = `${distDir.replace(resolve(distDir, '..'), '')}`;
        const prettyPath = page.replace(PAGES_DIR, '');
        const pageTimer = createTimer(`- ${defaultLanguage ? '' : subdir}${prettyPath}`);
        const path = resolve(page);
        const newPath = path.replace(PAGES_DIR, distDir).replace('.pug', '.html');
        const dir = newPath.replace(basename(newPath), '');
        const dirExists = await directoryExists(dir);

        if (!dirExists) {
          await retryWithDelay(() => mkdirp(dir));
        }

        const context = await getContext(environment, page);
        const contents = compileFile(path)({
          ctx: context,
          ...defaultContext,
        });

        await writeFile(newPath, contents, { encoding: 'utf-8' });

        pageTimer.finish();
      } catch (e) {
        error(e.message, e);

        throw e;
      }
    }),
  );

  timer.finish();

  return result;
}
