import { resolve } from 'path';
import * as yargs from 'yargs';
import { buildCommand } from './build.command';
import { Environments } from './build/environments.type';
import { initCommand } from './init.command';
import { serveCommand } from './serve.command';
import { sitemapCommand } from './sitemap.command';
import { startCommand } from './start.command';
import { watchCommand } from './watch.command';

const cwd = process.cwd();

function bail(err) {
  console.error(err);
  process.exit(-1);
}

export async function main(): Promise<void> {
  const self = yargs
    .command(
      'build [inputPath] [outputPath]',
      'Build your site',
      builder =>
        builder
          .positional('inputPath', {
            description: 'Path to look for source',
            default: './src',
          })
          .positional('outputPath', {
            description: 'Path to place generated site',
            default: './dist',
          })
          .option('env', {
            choices: ['production', 'development'],
            default: 'production' as Environments,
          })
          .option('base-url', {
            description: 'Base url of the site, in order to generate sitemap',
            type: 'string',
          })
          .option('post-hook', {
            description: 'Command that will be executed when finished successfully',
            type: 'array',
            string: true,
          }),
      args =>
        buildCommand(
          args.env,
          resolve(cwd, args.inputPath),
          resolve(cwd, args.outputPath),
          args['base-url'],
          args['post-hook'],
        ).catch(bail),
    )
    .command(
      'serve [inputPath]',
      'Serve up the site',
      builder =>
        builder
          .positional('inputPath', {
            description: 'Path to look for source',
            default: './dist',
          })
          .option('port', {
            type: 'number',
            default: 5000,
          }),
      args => serveCommand(resolve(cwd, args.inputPath), args.port),
    )
    .command(
      'watch [inputPath] [outputPath] [pattern]',
      'Watch for changes and build when detected',
      builder =>
        builder
          .positional('inputPath', {
            description: 'Path to look for source',
            default: './src',
          })
          .positional('outputPath', {
            description: 'Path to place generated site',
            default: './dist',
          })
          .positional('pattern', {
            description: 'Pattern relative to input path',
            default: './**/*',
          })
          .option('env', {
            choices: ['production', 'development'],
            default: 'production' as Environments,
          })
          .option('base-url', {
            description: 'Base url of the site, in order to generate sitemap',
            type: 'string',
          })
          .option('post-hook', {
            description: 'Command that will be executed when finished successfully',
            type: 'array',
            string: true,
          })
          .option('watch-dir', {
            description: 'Additional directory to watch',
            type: 'array',
            string: true,
          }),
      args =>
        watchCommand(
          args.env,
          resolve(cwd, args.inputPath),
          resolve(cwd, args.outputPath),
          args.pattern,
          () => Promise.resolve(),
          () => Promise.resolve(),
          args['base-url'],
          args['post-hook'],
          args['watch-dir'],
        ),
    )
    .command(
      'start [inputPath] [outputPath] [pattern]',
      'Watch and serve the site',
      builder =>
        builder
          .positional('inputPath', {
            description: 'Path to look for source',
            default: './src',
          })
          .positional('outputPath', {
            description: 'Path to place generated site',
            default: './dist',
          })
          .positional('pattern', {
            description: 'Pattern relative to input path',
            default: './**/*',
          })
          .option('port', {
            type: 'number',
            default: 5000,
          })
          .option('env', {
            choices: ['production', 'development'],
            default: 'production' as Environments,
          })
          .option('base-url', {
            description: 'Base url of the site, in order to generate sitemap',
            type: 'string',
          })
          .option('post-hook', {
            description: 'Command that will be executed when finished successfully',
            type: 'array',
            string: true,
          })
          .option('watch-dir', {
            description: 'Additional directory to watch',
            type: 'array',
            string: true,
          }),
      args =>
        startCommand(
          args.env,
          resolve(cwd, args.inputPath),
          resolve(cwd, args.outputPath),
          args.pattern,
          args.port,
          args['base-url'],
          args['post-hook'],
          args['watch-dir'],
        ),
    )
    .command(
      'init [outputPath]',
      'Bootstrap a new site',
      builder =>
        builder
          .positional('outputPath', {
            description: 'Path to new site',
            default: './src',
          })
          .positional('dry-run', {
            description: 'Dry run',
            default: false,
            type: 'boolean',
            alias: 'd',
          }),
      args => initCommand(resolve(cwd, args.outputPath)),
    )
    .command(
      'generate-sitemap [inputPath] [outputPath] [baseUrl]',
      'Build site and generate a sitemap.txt file',
      builder =>
        builder
          .positional('inputPath', {
            description: 'Path to look for source',
            default: './src',
          })
          .positional('outputPath', {
            description: 'Path to place generated site',
            default: './dist',
          })
          .positional('baseUrl', {
            description: 'Base url of the site',
            type: 'string',
          })
          .option('env', {
            choices: ['production', 'development'],
            default: 'production' as Environments,
          }),
      args =>
        sitemapCommand(args.baseUrl, args.env, resolve(cwd, args.inputPath), resolve(cwd, args.outputPath)).catch(bail),
    )
    .help()
    .wrap(120);
  const parsed = self.parse();

  if (parsed?._?.length === 0) {
    self.showHelp();
  }
}
