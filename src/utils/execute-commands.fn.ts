import { executeCommand } from './execute-command.fn';

export async function executeCommands(context: string, commands: string[]) {
  for (const command of commands) {
    await executeCommand(context, command);
  }
}
