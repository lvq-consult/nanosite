import { spawn } from 'child_process';
import { green, red, yellow } from 'colors';
import { EOL } from 'os';
import { contextualLogger } from '../build/log.fn';

export async function executeCommand(context: string, commands: string) {
  const { log, createTimer } = contextualLogger(context, yellow);
  const commandsSplit = commands.split('&&').map(c => c.trim());

  for (const command of commandsSplit) {
    const timer = createTimer();
    const commandSplit = command.split(' ');
    const [cmd, ...args] = commandSplit;

    await new Promise<void>(resolve => {
      log(`Spawning '${command}'`);
      const child = spawn(cmd, args, {});

      child.stdout.on('data', (data: Buffer) => {
        const lines = data.toString('utf-8').split(EOL);

        lines.filter(line => !!line).forEach(line => log(green(line)));
      });

      child.stderr.on('data', (data: Buffer) => {
        const lines = data.toString('utf-8').split(EOL);

        lines.filter(line => !!line).forEach(line => log(red(line)));
      });

      child.on('error', err => log(red(err.message)));

      child.on('close', code => {
        if (code !== 0) {
          log(red(`'${command}' exited with code ${code}`));
        }

        resolve();
      });
    });

    timer.finish();
  }
}
