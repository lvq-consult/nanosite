import { watch as chokidarWatch } from 'chokidar';
import { yellow } from 'colors';
import { resolve } from 'path';
import { fromEvent, merge } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { buildCommand } from './build.command';
import { Environments } from './build/environments.type';
import { contextualLogger } from './build/log.fn';

const { error } = contextualLogger('watch', yellow);

export async function watchCommand(
  environment: Environments,
  inputPath: string,
  outputPath: string,
  pattern: string,
  onChange: () => Promise<void>,
  afterBuild: (path: string) => Promise<void>,
  baseUrl?: string,
  postHooks?: string[],
  watchDirs?: string[],
  onReady?: () => Promise<void>,
) {
  let isBuilding = false;
  const build = async (path: string) => {
    isBuilding = true;
    await onChange();
    await buildCommand(environment, inputPath, outputPath, baseUrl, postHooks).catch(err => error('Build error', err));
    await afterBuild(path);
    isBuilding = false;
  };

  const paths = [resolve(inputPath, pattern), ...(watchDirs ?? []).map(path => resolve(path, './**/*'))];
  const watcher = chokidarWatch(paths);

  merge(
    fromEvent(watcher, 'add'),
    fromEvent(watcher, 'addDir'),
    fromEvent(watcher, 'change'),
    fromEvent(watcher, 'unlink'),
    fromEvent(watcher, 'unlinkDir'),
  )
    .pipe(
      filter(() => !isBuilding),
      switchMap(([path]) => build(path as string)),
    )
    .subscribe();

  watcher.on('error', err => error('Error', err));

  await onReady();
}
