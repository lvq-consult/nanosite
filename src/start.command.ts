import { cyan } from 'colors';
import { Environments } from './build/environments.type';
import { contextualLogger } from './build/log.fn';
import { livereloadCommand } from './livereload.command';
import { serveCommand } from './serve.command';
import { watchCommand } from './watch.command';

const { log } = contextualLogger('start', cyan);

export function startCommand(
  environment: Environments,
  inputPath: string,
  outputPath: string,
  pattern: string,
  port = 5000,
  baseUrl: string,
  postHooks?: string[],
  watchDirs?: string[],
) {
  const fileserver = serveCommand(outputPath, port);
  const livereloadServer = livereloadCommand(outputPath);

  watchCommand(
    environment,
    inputPath,
    outputPath,
    pattern,
    async () => {
      log('Detected change, pausing file serving');

      await fileserver.stop();
    },
    async () => {
      log('Resume serving files');

      await fileserver.listen();
      livereloadServer.refresh('');
    },
    baseUrl,
    postHooks,
    watchDirs,
    async () => {
      await fileserver.listen();
      await livereloadServer.listen();
    },
  );
}
