import { white } from 'colors';
import { copyFile, remove } from 'fs-extra';
import { trimEnd } from 'lodash';
import { resolve } from 'path';
import { buildCommand } from './build.command';
import { Environments } from './build/environments.type';
import { generateSitemap } from './build/generate-sitemap.fn';
import { contextualLogger } from './build/log.fn';

const { error } = contextualLogger('🗺️ sitemap', white);

export async function sitemapCommand(
  baseUrl: string,
  environment: Environments,
  inputPath: string,
  outputPath: string,
) {
  const sitemapOutputPath = `${trimEnd(outputPath, '/')}-sitemap`;

  if (!new RegExp('^https?://').test(baseUrl)) {
    error(`Invalid base url '${baseUrl}' it should be prefixed with either http:// or https://`);
    process.exit(-1);
  }

  try {
    await buildCommand(environment, inputPath, sitemapOutputPath);
  } catch (e) {
    error(e);
    process.exit(-1);
  }

  await generateSitemap(baseUrl, sitemapOutputPath);

  const sitemapResultFile = resolve(sitemapOutputPath, 'sitemap.txt');

  await copyFile(sitemapResultFile, resolve(inputPath, 'assets', 'sitemap.txt'));

  await remove(sitemapOutputPath);
}
