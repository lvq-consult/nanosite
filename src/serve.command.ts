import { green } from 'colors';
import { once } from 'events';
import { createServer } from 'http';
import { contextualLogger } from './build/log.fn';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const handler = require('serve-handler');

const { log, createTimer } = contextualLogger('serve', green);

export function serveCommand(inputPath: string, port = 5000) {
  const server = createServer(async (request, response) => {
    const timer = createTimer();
    const result = await handler(request, response, { public: inputPath });

    log(`${request.method} ${request.url} ${timer.elapsed}ms`);

    return result;
  });

  let hasSeenListeningEvent = false;

  return {
    async listen() {
      if (hasSeenListeningEvent) {
        await new Promise<void>(resolve => server.close(() => resolve()));
      }

      return new Promise<void>(resolve => {
        hasSeenListeningEvent = false;

        once(server, 'listening').then(() => {
          hasSeenListeningEvent = true;
          log(`serving ${inputPath} @ http://localhost:${port}`);
          resolve();
        });

        server.listen(port);
      });
    },
    async stop() {
      return new Promise<void>(resolve => {
        once(server, 'close').then(() => resolve());

        if (hasSeenListeningEvent) {
          server.close();
        } else {
          server.on('listening', () => server.close());
        }
      });
    },
  };
}
