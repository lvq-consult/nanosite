const { default: axios } = require('axios');
const { readFile } = require('fs/promises');

async function main() {
  const content = await readFile('./.gitlab-ci.yml', { encoding: 'utf-8' });

  const res = await axios.post(
    'https://gitlab.com/api/v4/ci/lint',
    {
      content,
      include_merged_yaml: true,
      include_jobs: true,
    },
    {
      headers: {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': 'glpat-vLNSermySELG4cMZzrLh',
      },
    },
  );

  if (res.status !== 200) {
    console.error(res.statusText);
    process.exit(-1);
  }

  if (res.data.status === 'valid' && res.data.warnings.length === 0) {
    console.log('.gitlab-ci.yml is valid');
    process.exit(0);
  }

  if (res.data.status === 'valid' && res.data.warnings.length > 0) {
    console.log('.gitlab-ci.yml had warnings');
    res.data.warnings.forEach(warning => console.warn(warning));
    process.exit(-1);
  }

  if (res.data.status === 'invalid') {
    console.log('.gitlab-ci.yml had error');
    res.data.errors.forEach(warning => console.error(warning));
    process.exit(-1);
  }
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(-1);
  });
